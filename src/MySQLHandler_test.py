# coding=UTF-8

import unittest
import datetime
import ConfigParser

from MySQLHandler import MySQLHandler


class MySQLHandlerTest(unittest.TestCase):
    def test_getAllStoryNames(self):
        database = self.createDBItem()
        self.assertEquals(['Ichi', 'Ni', 'Sanasdf'], database.getAllStoryNames())   
    
    def test_getStoryIdByName(self):
        database = self.createDBItem()
        self.assertEquals("6gmtet", database.getStoryIdByName("Ichi"))
        self.assertEquals("", database.getStoryIdByName("invali"))
    
    def test_getStoryNameById(self):
        database = self.createDBItem()
        self.assertEquals("Ichi", database.getStoryNameById("6gmtet"))
        self.assertEquals("", database.getStoryNameById("invali"))

    def test_checkIfStoryExistsById(self):
        database = self.createDBItem()
        self.assertEquals(True, database.checkIfStoryExistsById('6gmtet'))
        self.assertEquals(False, database.checkIfStoryExistsById('invali'))

    def test_checkIfStoryExistsByName(self):
        database = self.createDBItem()
        self.assertEquals(True, database.checkIfStoryExistsByName('Ichi'))
        self.assertEquals(False, database.checkIfStoryExistsByName('Invalid Name'))

    def test_checkIfPartExistsById(self):
        database = self.createDBItem()
        self.assertEquals(True, database.checkIfPartExistsById('6gmtet'))
        self.assertEquals(False, database.checkIfPartExistsById('invali'))

    def test_getPartNameById(self):
        database = self.createDBItem()
        self.assertEquals('Ichi: Part 2', database.getPartNameById('asdfgh'))
        self.assertEquals('', database.getPartNameById('invali'))
    
    def test_getLatestNrOfStoryById(self):
        database = self.createDBItem()
        self.assertEquals("3", database.getLatestNrOfStoryById("6gmtet"))
        self.assertEquals("", database.getLatestNrOfStoryById("invali"))
    
    def test_getLatestIdOfStoryById(self):
        database = self.createDBItem()
        self.assertEquals("asdfgj", database.getLatestIdOfStoryById("6gmtet"))
        self.assertEquals("", database.getLatestIdOfStoryById("invali"))

    def test_getSubscribersByStoryId(self):
        database = self.createDBItem()
        self.assertEquals(["Test_Subscriber"], database.getSubscribersByStoryId("6gmtet"))
        self.assertEquals([], database.getSubscribersByStoryId("invali"))
    
    def test_getSubscriptionsBySubscriberName(self):
        database = self.createDBItem()
        self.assertEquals(['6gmtet'], database.getSubscriptionsBySubscriberName('Test_Subscriber'))
        self.assertEquals([], database.getSubscriptionsBySubscriberName('Invalid_User'))
    
    def test_getSubscriberByName(self):
        database = self.createDBItem()
        self.assertEquals('Test_Subscriber', database.getSubscriberByName('Test_Subscriber'))
        self.assertEquals('', database.getSubscriberByName('Invalid_User'))
    
    def test_checkIfSubscriberExists(self):
        database = self.createDBItem()
        self.assertEquals(True, database.checkIfSubscriberExists("Test_Subscriber"))

    def test_checkIfSubscriberHasSubscription(self):
        database = self.createDBItem()
        self.assertEquals(True, database.checkIfSubscriberHasSubscription('Test_Subscriber', '6gmtet'))
        self.assertEquals(False, database.checkIfSubscriberHasSubscription('Invalid User', 'invali'))
    
    def test_getStoryAuthorById(self):
        database = self.createDBItem()
        self.assertEquals('TestAutor', database.getStoryAuthorById('6gmtet'))
        self.assertEquals('', database.getStoryAuthorById('invali'))

    def test_SubscriberAdditionAndDeletion(self):
        database = self.createDBItem()
        name = "SubscriberForTest"
        try:
            self.assertEquals(False, database.checkIfSubscriberExists(name))
            database.addSubscriber(name)
            self.assertEquals(True, database.checkIfSubscriberExists(name))
        finally:
            database.deleteSubscriber(name)
            self.assertEquals(False, database.checkIfSubscriberExists(name))

    def test_SubscriptionAndUnsubscription(self):
        database = self.createDBItem()
        name = "Test_Subscriber"
        id = "6gmtil"
        try:
            self.assertEquals(False, database.checkIfSubscriberHasSubscription(name, id))
            database.subscribeToStory(name, id)
            self.assertEquals(True, database.checkIfSubscriberHasSubscription(name, id))
        finally:
            database.unsubscribeFromStory(name, id)
            self.assertEquals(False, database.checkIfSubscriberHasSubscription(name, id))

    def test_StoryAdditionAndDeletion(self):
        database = self.createDBItem()
        id = 'tzuiop'
        name = 'Test-Story 1'
        date = datetime.datetime.now()
        author = 'Test-Autor'
        commentId = 'jojojo'
        try:
            database.addStory(id, name, date, author, commentId)
            self.assertEquals(id, database.getStoryIdByName(name))
            self.assertEquals(name, database.getStoryNameById(id))
            self.assertEquals(name, database.getPartNameById(id))
        finally:
            database.deleteStory(id)
            self.assertEquals('', database.getStoryNameById(id))
            self.assertEquals('', database.getPartNameById(id))
       
    def test_PartAdditionAndDeletion(self):
        database = self.createDBItem()
        story_id = "6gmtet"
        cont_id = "tyuiop"
        cont_title = "Test-Titel"
        cont_nr = "4"
        cont_date = datetime.datetime.now()
        commentId = 'jojojo'
        try:
            self.assertEquals("3", database.getLatestNrOfStoryById(story_id))
            self.assertEquals(False, database.checkIfPartExistsById(cont_id))
            database.addStoryContinuation(story_id, cont_id, cont_title, cont_nr, cont_date, commentId)
            self.assertEquals(True, database.checkIfPartExistsById(cont_id))
        finally:
            database.deletePart(cont_id)
            self.assertEquals(False, database.checkIfPartExistsById(cont_id))



    def createDBItem(self):
        config = ConfigParser.RawConfigParser()
        config.read('settings_test.ini')

        dbIP = config.get('MySQL', 'ip')
        dbuser = config.get('MySQL', 'user')
        dbpassword = config.get('MySQL', 'password')
        dbname = config.get('MySQL', 'db')
        database = MySQLHandler(dbIP, dbuser, dbpassword, dbname)
        return database

if __name__ == '__main__':
    unittest.main()
