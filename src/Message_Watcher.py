import ConfigParser
import praw
import re

from MySQLHandler import MySQLHandler


def main():
    global reStoryId 
    reStoryId = re.compile(ur"^[A-Za-z0-9]{6}$")

    global config 
    config = ConfigParser.RawConfigParser()
    config_file_path = 'settings.ini'
    config.read(config_file_path)

    dbIP = config.get('MySQL', 'ip')
    dbuser = config.get('MySQL', 'user')
    dbpassword = config.get('MySQL', 'password')
    dbname = config.get('MySQL', 'db')
    global database 
    database = MySQLHandler(dbIP, dbuser, dbpassword, dbname)
    global reddit 
    reddit = praw.Reddit(
        config.get('reddit', 'botname'), user_agent=config.get('reddit', 'user_agent'))

    print "Monitoring Inbox"

    actions = {
        "Subscription": message_subscription,
        "Subscribe": message_subscription,
        "Unsubscribe": message_unsubscription,
        "Unsubscribe From Everything": message_unsubscribeFromEverything
    }

    for mail in reddit.inbox.stream():
        print mail.subject + " from " + mail.author.name
        # Ausfuehren, wenn eine Nachricht mit Titel "Subscription" eingeht
        try:
            actions[mail.subject](mail)
        except KeyError:
            reply(mail, "Unknown Task. Possible Actions are `Subscribe`, `Unsubscribe` and `Unsubscribe from Everything`")
        # TODO: Administrative actions.
        mail.mark_read()


def message_subscription(mail):
    # TODO: Möglich machen, auch via StoryName + Autor zu abonnieren
    global database
    print("Subscription requested")
    checkUser(mail.author.name)
    storyId = getStoryId(mail.body)
    storyName = database.getStoryNameById(storyId)
    storyAuthor = database.getStoryAuthorById(storyId)
    if storyId == -1:
        reply(mail, "The message contained more than a Story ID  \nPlease try again with only the Story ID or contact The Creator if you think this was an error.")
        return
    # Pruefe ob der Benutzer diese Story noch nicht abonniert hat.
    if not database.checkIfSubscriberHasSubscription(mail.author.name, storyId):
        print "User is not subscribed to this Story yet, proceeding"
        if database.checkIfStoryExistsById(storyId):
            print "story exists, proceeding"
            database.subscribeToStory(mail.author.name, storyId)
            if database.checkIfSubscriberHasSubscription(mail.author.name, storyId):
                reply(mail, "You have been subscribed to the Story " + storyName + " by /u/" + storyAuthor + 
                    ".  \nIf you want to unsubscribe, [click here](https://reddit.com/message/compose/?to=" + 
                    reddit.user.me().name + "&subject=Unsubscribe&message="+storyId+")  \n" + 
                    "If you want to unsubscribe from all Stories, [click here](https://reddit.com/message/compose/?to=" + 
                    reddit.user.me().name + "&subject=Unsubscribe From Everything&message=UnsubscribeFromEverything)")
                print "User has been subscribed"
            else:
                reply(mail, "Something went wrong, please try again later.  \nIf this error persists, please contact The Creator.")
    else:
        reply(mail, "You are already subscribed to the Story " + storyName + " by /u/" + storyAuthor)

def message_unsubscription(mail):
    # TODO: Möglich machen, auch via StoryName + Autor zu entabonnieren
    global database
    storyId = getStoryId(mail.body)
    storyName = database.getStoryNameById(storyId)
    storyAuthor = database.getStoryAuthorById(storyId)
    if storyId == -1:
        reply(mail, "The message contained more than a Story ID  \nPlease try again with only the Story ID or contact The Creator if you think this was an error.")
        return
    if not database.checkIfSubscriberHasSubscription(mail.author.name, storyId):
        reply(mail, "You are not subscribed to the Story " + storyName + " by /u/" + storyAuthor + ".")
        return
    database.unsubscribeFromStory(mail.author.name, storyId)
    if not database.checkIfSubscriberHasSubscription(mail.author.name, storyId):
        reply(mail, "You have been unsubscribed from the Story " + storyName + " by /u/"+ storyAuthor +".  \n" + 
            "If you want to unsubscribe from all Stories, [click here](https://reddit.com/message/compose/?to=" + 
            reddit.user.me().name + "&subject=Unsubscribe From Everything&message=UnsubscribeFromEverything)")
    else:
        reply(mail, "Something went wrong, Please try again later.  \nIf this error persists, please contact The Creator.")

def message_unsubscribeFromEverything(mail):
    global database
    if database.checkIfSubscriberExists(mail.author.name):
        database.deleteSubscriber(mail.author.name)
        if not database.checkIfSubscriberExists(mail.author.name):
            reply(mail, "You have been unsubscribed from all Stories.")
        else:
            reply(mail, "Something went wrong, please try again later.  \nIf this error persists, please contact The Creator.")
    else:
        reply(mail, "You are already not subscribed to anything.")
        

def reply(item, message):
    """
    Appends the configured footer before replying

    :param item: expects the reddit item (a comment, message or post) the reply is adressed at
    :param message: expects the message that is supposed to be sent
    """
    global reddit
    global config
    footer = config.get('messages', 'footer')
    item.reply(message + '\n\n' + footer)
    return



def checkUser(user_name):
    """
    Checks if a user exists in the database. If it doesn't, it's created.
    
    :param user_name: Expects a reddit user name.
    """
    global database
    if database.checkIfSubscriberExists(user_name):
        return
    else:
        database.addSubscriber(user_name)

def getStoryId(text):
    global reStoryId
    matches = reStoryId.search(text)
    if matches:
        return text
    else:
        return -1


if __name__ == '__main__':
    main()
