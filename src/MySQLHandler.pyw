import MySQLdb as mdb


class MySQLHandler(object):
    def __init__(self, ip, user, password, db):
        self.db_ip = ip
        self.db_user = user
        self.db_password = password
        self.db_name = db

    # Stories
    def getAllStoryNames(self):
        result = []
        for item in self.get("SELECT Name FROM Stories", ''):
            result.append(''.join(item))
        return result

    def checkIfStoryExistsById(self, id):
        result = self.joinIfValid(self.get("SELECT EXISTS(SELECT * FROM Stories WHERE id = %s)", id))
        if result == '1':
            return True
        return False

    def checkIfStoryExistsByName(self, name):
        result = self.joinIfValid(self.get("SELECT EXISTS(SELECT * FROM Stories WHERE Name = %s)", name))
        if result == '1':
            return True
        return False

    def getStoryIdByName(self, name):
        return self.joinIfValid(self.get("SELECT id FROM Stories WHERE Name = %s", name))

    def getStoryNameById(self, id):
        return self.joinIfValid(self.get("SELECT Name FROM Stories WHERE id = %s", id))

    def getStoryAuthorById(self, id):
        return self.joinIfValid(self.get("SELECT Author FROM Stories WHERE id = %s", id))


    # Parts
    def checkIfPartExistsById(self, id):
        result = self.joinIfValid(self.get("SELECT EXISTS( SELECT * FROM Parts WHERE id = %s)", id))
        if result == '1':
            return True
        return False

    def getPartNameById(self, id):
        return self.joinIfValid(self.get("SELECT Title FROM Parts WHERE id = %s", id))

    def getLatestNrOfStoryById(self, id):
        return self.joinIfValid(self.get("SELECT Nr FROM Parts WHERE Stories_id = %s ORDER BY Nr DESC LIMIT 1", id))

    def getLatestIdOfStoryById(self, id):
        return self.joinIfValid(self.get("SELECT id FROM Parts WHERE Stories_id = %s ORDER BY NR DESC LIMIT 1", id))

    # Subscribers
    def checkIfSubscriberExists(self, name):
        result = self.joinIfValid(self.get("SELECT EXISTS(SELECT Name FROM Subscribers WHERE Name = %s)", name))
        if result == '1':
            return True
        return False

    def getSubscribersByStoryId(self, id):
        result = []
        for item in self.get("SELECT Subscribers_Name FROM Subscribers_has_Stories WHERE Stories_id = %s", id):
            result.append(''.join(item))
        return result

    def getSubscriptionsBySubscriberName(self, name):
        result = []
        for item in self.get("SELECT Stories_id FROM Subscribers_has_Stories WHERE Subscribers_Name = %s", name):
            result.append(''.join(item))
        return result

    def getSubscriberByName(self, name):
        return self.joinIfValid(self.get("SELECT Name FROM Subscribers WHERE Name = %s", name))

    # Subscriptions
    def checkIfSubscriberHasSubscription(self, subscriber, storyId):
        result = self.joinIfValid(self.getWithTuple("SELECT EXISTS(SELECT * FROM Subscribers_has_Stories WHERE Subscribers_Name = %s AND Stories_id = %s)", (subscriber, storyId)))
        if result == '1':
            return True
        return False

    # Additions
    def addStory(self, id, title, date, author, commentId):
        self.insert("INSERT INTO Stories(id, Name, LastUpdate, Author) VALUES(%s,%s,%s,%s)", [
                    id, title, date, author])
        self.insert("INSERT INTO Parts(id, Title, Nr, Stories_id, Date, Comment_id) VALUES(%s,%s,%s,%s,%s,%s)", [
                    id, title, 1, id, date, commentId])

    def addStoryContinuation(self, story_id, cont_id, cont_title, cont_nr, cont_date, commentId):
        self.insert("INSERT INTO Parts(id, Title, Nr, Stories_id, Date, Comment_id) VALUES(%s, %s, %s, %s, %s, %s)", [
                    cont_id, cont_title, cont_nr, story_id, cont_date, commentId])
    
    def addSubscriber(self, name):
        self.insert("INSERT INTO Subscribers(Name) VALUE(%s)", name)
    
    def subscribeToStory(self, subscriber, story):
        self.insert("INSERT INTO Subscribers_has_Stories(Subscribers_Name, Stories_id) VALUES(%s, %s)", [subscriber, story])

    # Removals
    def deleteStory(self, id):
        self.delete("DELETE FROM Stories WHERE id = %s", id)
    
    def deletePart(self, id):
        self.delete("DELETE FROM Parts WHERE id = %s", id)
    
    def deleteSubscriber(self, name):
        self.delete("DELETE FROM Subscribers WHERE Name = %s", name)

    def unsubscribeFromStory(self, subscriber, story):
        self.delete("DELETE FROM Subscribers_has_Stories WHERE Subscribers_Name = %s AND Stories_id = %s", [subscriber, story])

    # DB Connections
    def insert(self, string, values):
        try:
            con = mdb.connect(host = self.db_ip, user = self.db_user,
                              passwd = self.db_password, db = self.db_name, charset = "utf8")
            cur = con.cursor()
            cur.execute(string, values)
            con.commit()
        except mdb.Error, e:
            if con:
                con.rollback()
            print "Error %d: %s" % (e.args[0], e.args[1])
            # sys.exit(1); #Ersatz finden - Logfile?
        finally:
            if con:
                con.close()

    def get(self, string, input):
        try:
            con = mdb.connect(host = self.db_ip, user = self.db_user,
                              passwd = self.db_password, db = self.db_name, charset = "utf8")
            cur = con.cursor()
            if input is not '':
                cur.execute(string, (input,))
            else:
                cur.execute(string)
            rows = cur.fetchall()
        except mdb.Error, e:
            if con:
                con.rollback()
            print "Error %d: %s" % (e.args[0], e.args[1])
        # sys.exit(1); #Ersatz finden - Logfile?
        finally:
            if con:
                con.close()
        return rows

    def getWithTuple(self, string, input):
        try:
            con = mdb.connect(host = self.db_ip, user = self.db_user,
                              passwd = self.db_password, db = self.db_name, charset = "utf8")
            cur = con.cursor()
            cur.execute(string, input)
            rows = cur.fetchall()
        except mdb.Error, e:
            if con:
                con.rollback()
            print "Error %d: %s" % (e.args[0], e.args[1])
        # sys.exit(1); #Ersatz finden - Logfile?
        finally:
            if con:
                con.close()
        return rows

    def delete(self, string, values):
        try:
            con = mdb.connect(host = self.db_ip, user = self.db_user,
                              passwd = self.db_password, db = self.db_name, charset = "utf8")
            cur = con.cursor()
            cur.execute(string, values)
            con.commit()
        except mdb.Error, e:
            if con:
                con.rollback()
            print "Error %d: %s" % (e.args[0], e.args[1])
            # sys.exit(1); #Ersatz finden - Logfile?
        finally:
            if con:
                con.close()


    """
    Returns a given tuple as string or an empty string if the tuple is emtpy
    """

    def joinIfValid(self, tuple):
        if tuple is not ():
            return ''.join(map(str, tuple[0]))
        return ''
