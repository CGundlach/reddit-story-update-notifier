#coding= utf-8

import re
import sys
import time
import datetime
import ConfigParser
import praw
from MySQLHandler import MySQLHandler


def main():
    global config
    config = ConfigParser.RawConfigParser()
    config_file_path = 'settings.ini'
    config.read(config_file_path)
    global footer, author
    footer = config.get('messages', 'footer')
    author = config.get('messages', 'author')
    dbIP = config.get('MySQL', 'ip')
    dbuser = config.get('MySQL', 'user')
    dbpassword = config.get('MySQL', 'password')
    dbname = config.get('MySQL', 'db')
    global database
    database = MySQLHandler(dbIP, dbuser, dbpassword, dbname)
    global reddit
    reddit = praw.Reddit(
        config.get('reddit', 'botname'), user_agent=config.get('reddit', 'user_agent'))
    subreddit = reddit.subreddit(config.get('general', 'subreddit'))

    print("monitoring subreddit " + subreddit.fullname)
    for submission in subreddit.stream.submissions():
        print(submission.title.encode('utf8'))
        if "Update" not in submission.title:
            threadExists = False
            if not database.checkIfPartExistsById(submission.id):
                story_id = getStory(submission)
                if story_id:
                    print "Belonging to story id " + story_id
                    commentId = post_top_comment_cont(submission, story_id)
                    addContinuation(story_id, submission, commentId)
                    # TODO: Frühere Kommentare bearbeiten und Link zum nächsten Teil hinzufügen
                    notify_subscribers(story_id)
                else:
                    print "adding new story"
                    commentId = post_top_comment_story(submission)
                    addStory(submission, commentId)

def notify_subscribers(story_id):
    """
    Notifies all subscribers to a story of a new continuation.
    :param story_id: Expects a story id.
    """
    global database, reddit, footer
    story_title = database.getStoryNameById(story_id)
    subscribers = database.getSubscribersByStoryId(story_id)
    story_latest = database.getLatestIdOfStoryById(story_id)
    for subscriber in subscribers:
        recipient = ''.join(subscriber[0])
        print "notifying " + recipient
        reddit.redditor(recipient).message("Update to the story " + story_title, 'There has been an update to the Story "' +
                                           story_title + '". [Click here to read!](https://redd.it/' + story_latest + ')' + footer)

def getStory(submission):
    """
    Determines if a submission is part of an ongoing story and returns the story's id if it is.  
    :param submission: expects a submission object  
    :returns: either the parent story id or false if no parent story is found
    """
    global database
    storyList = database.getAllStoryNames()
    wordlist = submission.title.split(':')[0].split()
    title = ""
    for word in wordlist:
        title = title + " " + word
        title = re.sub(r"^\s+", "", title)
        for entry in storyList:
            if entry.lower() == title.lower():
                return database.getStoryIdByName(title)
    return False

def post_top_comment_cont(submission, story_id):
    """
    Posts the top comment on a submission that is part of an existing story.  
    It contains a subscription link to be reminded of further posts to this story and links to 
    previous parts of this story.
    :param submission: takes a submission object
    :param story_id: takes a story id  
    :return ID des geposteten Kommentars oder '-1', wenn kein Kommentar gepostet wurde.
    """
    if submission.archived:
        return '-1'
    global database, reddit, author, footer
    story_title = database.getStoryNameById(story_id)
    return submission.reply("This is part of the story " + story_title +
                     ". [Part 1 of this story is here](https://redd.it/" + story_id +
                     ") and the [previous part is here](https://redd.it/" + get_latest(story_id) +
                     ").   \n\n If you wish to be notified of any updates to this story, [click this link to send me a PM](https://reddit.com/message/compose/?to=" + reddit.user.me().name +
                     "&subject=Subscribe&message="+story_id +
                     "). Do not alter the message." + author + footer).id
    # TODO extend comment content with the five previous parts

def post_top_comment_story(submission):
    """
    Posts the top comment on a submission that is not part of an existing story. The top comment contains a subscription link to be reminded of further posts to this story.
    :param submission: expects a submission object  
    :return ID des geposteten Kommentars oder '-1', wenn kein Kommentar gepostet wurde.
    """
    if submission.archived:
        return '-1'
    global author, footer
    return submission.reply("This is a new story or oneshot.  \n" +
                     "If you wish to be notified of any updates, [click this link to send me a PM](https://reddit.com/message/compose/?to=" + reddit.user.me().name +
                     "&subject=Subscribe&message="+submission.id+" ). Do not alter the message.\n\n" + footer).id

def addContinuation(story_id, submission, commentId):
    """
    Takes a story id and  a submission object and adds that submission as a continuation of the story.
    :param story_id: expects a story id  
    :param submission: expects a submission object  
    """
    global database
    try: 
        number = int(database.getLatestNrOfStoryById(story_id))+1
    except error:
        print error
        number = 0
    database.addStoryContinuation(story_id, submission.id, submission.title, number, str(
        datetime.datetime.fromtimestamp(submission.created)), commentId)


def addStory(submission, commentId):
    """
    Takes a submission object and creates a new story from it. The submission title is the new title for the story.
    :param submission: expects a submission object
    """
    global database
    database.addStory(submission.id, submission.title, str(
        datetime.datetime.fromtimestamp(submission.created)), submission.author, commentId)


if __name__ == '__main__':
    main()
