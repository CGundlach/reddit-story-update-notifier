CREATE DATABASE  IF NOT EXISTS `testdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `testdb`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 85.214.125.121    Database: testdb
-- ------------------------------------------------------
-- Server version	5.5.60-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Parts`
--

DROP TABLE IF EXISTS `Parts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Parts` (
  `id` varchar(10) NOT NULL,
  `Nr` int(11) NOT NULL,
  `Stories_id` varchar(10) NOT NULL,
  `Title` varchar(45) NOT NULL,
  `Date` datetime NOT NULL,
  `Comment_id` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Continuations_Stories1_idx` (`Stories_id`),
  CONSTRAINT `fk_Continuations_Stories1` FOREIGN KEY (`Stories_id`) REFERENCES `Stories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Parts`
--

LOCK TABLES `Parts` WRITE;
/*!40000 ALTER TABLE `Parts` DISABLE KEYS */;
INSERT INTO `Parts` VALUES ('6gmtet',1,'6gmtet','Ichi','2017-06-12 04:05:32','rebede'),('6gmtil',1,'6gmtil','Ni','2017-06-12 04:05:56','rabada'),('6pe7pd',1,'6pe7pd','Sanasdf','2017-07-25 15:44:11','kululu'),('asdfgh',2,'6gmtet','Ichi: Part 2','2018-06-02 00:00:00','rarara'),('asdfgj',3,'6gmtet','Ichi: Part 3','0000-00-00 00:00:00','hehehe');
/*!40000 ALTER TABLE `Parts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Stories`
--

DROP TABLE IF EXISTS `Stories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Stories` (
  `id` varchar(10) NOT NULL,
  `Name` tinytext NOT NULL,
  `LastUpdate` datetime NOT NULL,
  `Author` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Stories`
--

LOCK TABLES `Stories` WRITE;
/*!40000 ALTER TABLE `Stories` DISABLE KEYS */;
INSERT INTO `Stories` VALUES ('6gmtet','Ichi','2017-06-12 04:05:32','TestAutor'),('6gmtil','Ni','2017-06-12 04:05:56','TestAutor2'),('6pe7pd','Sanasdf','2017-07-25 15:44:11','TestAutor');
/*!40000 ALTER TABLE `Stories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Subscribers`
--

DROP TABLE IF EXISTS `Subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Subscribers` (
  `Name` varchar(25) NOT NULL,
  PRIMARY KEY (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Subscribers`
--

LOCK TABLES `Subscribers` WRITE;
/*!40000 ALTER TABLE `Subscribers` DISABLE KEYS */;
INSERT INTO `Subscribers` VALUES ('Test_Subscriber');
/*!40000 ALTER TABLE `Subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Subscribers_has_Stories`
--

DROP TABLE IF EXISTS `Subscribers_has_Stories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Subscribers_has_Stories` (
  `Subscribers_Name` varchar(25) NOT NULL,
  `Stories_id` varchar(10) NOT NULL,
  PRIMARY KEY (`Subscribers_Name`,`Stories_id`),
  KEY `fk_Subscribers_has_Stories_Stories1_idx` (`Stories_id`),
  KEY `fk_Subscribers_has_Stories_Subscribers1_idx` (`Subscribers_Name`),
  CONSTRAINT `fk_Subscribers_has_Stories_Subscribers1` FOREIGN KEY (`Subscribers_Name`) REFERENCES `Subscribers` (`Name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Subscribers_has_Stories_Stories1` FOREIGN KEY (`Stories_id`) REFERENCES `Stories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Subscribers_has_Stories`
--

LOCK TABLES `Subscribers_has_Stories` WRITE;
/*!40000 ALTER TABLE `Subscribers_has_Stories` DISABLE KEYS */;
INSERT INTO `Subscribers_has_Stories` VALUES ('Test_Subscriber','6gmtet');
/*!40000 ALTER TABLE `Subscribers_has_Stories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'testdb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-21  7:30:04
